const fs = require('fs')
    , filename = process.argv[2]

fs.watch(filename, function() {
  console.log(filename + ' changed!')
})

console.log('Now watching %s', filename)