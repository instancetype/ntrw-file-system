/**
 * Created by instancetype on 6/28/14.
 */
const fs = require('fs')
    , script = process.argv[1]
    , filename = process.argv[2]

if (!filename) throw Error('Must specify a file to watch!')

fs.watch(filename, function() {
  console.log('File ' + filename + ' changed.')
})

console.log('The script at ' + script + ' is now watching '
             + filename + ' for changes...')